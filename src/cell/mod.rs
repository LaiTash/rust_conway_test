pub type ConwayCellCoord = usize;

#[derive(Clone, Copy, Debug, PartialEq, Eq, Hash)]
pub struct Rect {
    pub x: ConwayCellCoord,
    pub y: ConwayCellCoord,
    pub width: ConwayCellCoord,
    pub height: ConwayCellCoord
}


impl Rect {
    pub fn contains(&self, cell: &ConwayCell) -> bool {
        cell.x >= self.x && cell.y >= self.y &&
            cell.x - self.x <= self.width && cell.y - self.y <= self.height
    }

    pub fn x2(&self) -> usize { self.x + self.width }
    pub fn y2(&self) -> usize { self.y + self.height }
}


#[derive(Hash, Eq, PartialEq, Debug, Copy, Clone)]
pub struct ConwayCell {
    pub x: ConwayCellCoord,
    pub y: ConwayCellCoord
}


impl From<(usize, usize)> for ConwayCell {
    fn from(value: (usize, usize)) -> ConwayCell {
        let (x, y) = value;
        ConwayCell { x: x, y: y }
    }
}

