extern crate ncurses;

use state::state::StateInterface;
use cell::ConwayCellCoord;
use cell::Rect;
use std::time::{SystemTime};
use self::ncurses::*;


struct CursesConwayRenderer {
    x: ConwayCellCoord,
    y: ConwayCellCoord,
    window: WINDOW,
}

impl CursesConwayRenderer {
    fn render_loop(&self, starting_state: Box<StateInterface>) {
        let mut last_tagged_generation = 0;
        let mut generation = 0;
        let mut started_at = SystemTime::now();
        let mut states_per_second = 0;

        println!("{:?}", starting_state.get_base().generation);
        for state in starting_state {

            let elapsed = started_at.elapsed().unwrap();
            let elapsed_as_sec = elapsed.as_secs();
            if elapsed_as_sec >= 10 {
                states_per_second = (generation - last_tagged_generation) / elapsed_as_sec;
                last_tagged_generation = generation;
                started_at = SystemTime::now();
            }
            generation = state.get_base().generation;
            self.render_state(&*state);
            wmove(self.window, 2, 2);
            wprintw(self.window, &format!("Generation: {}", generation));
            wmove(self.window, 3, 2);
            wprintw(self.window, &format!("Elapsed: {}", elapsed.as_secs()));
            wmove(self.window, 4, 2);
            wprintw(self.window, &format!("States per second: {}", states_per_second));
            refresh();
        }
    }

    fn render_state(&self, state: &StateInterface) {
        let mut win_width = 0;
        let mut win_height = 0;

        wclear(self.window);

        getmaxyx(self.window, &mut win_height, &mut win_width);

        let rect = Rect {
            x: self.x,
            y: self.y,
            width: win_width as ConwayCellCoord,
            height: win_height as ConwayCellCoord
        };

        for cell in state.iter_cells_in_rect(rect) {
            wmove(self.window, (cell.y - self.y) as i32, (cell.x - self.x) as i32);
            waddch(self.window, '@' as chtype);
        }
    }
}


pub struct CursesRenderer {
    base: CursesConwayRenderer
}


impl CursesRenderer {
    pub fn new() -> CursesRenderer {
        CursesRenderer {
            base: CursesConwayRenderer {
                x: 0, y:0, window: initscr()
            }
        }
    }


    pub fn render_loop(&self, starting_state: Box<StateInterface>) {
        self.base.render_loop(starting_state);
    }
}


impl Drop for CursesRenderer {
    fn drop(&mut self) {
        endwin();
    }
}