extern crate piston;
extern crate graphics;
extern crate opengl_graphics;
extern crate piston_window;
extern crate glutin_window;

use self::piston::window::{Window, OpenGLWindow};
use self::graphics::{clear, rectangle, Context, Transformed};

use self::piston_window::{PistonWindow, G2d};
use self::glutin_window::GlutinWindow;


use state::state::StateInterface;
use cell::Rect;
use cell::ConwayCell;

use std::cell::RefCell;


pub mod renderer_settings {
    use cell::ConwayCell;
    use super::graphics::types::Color;


    const DEFAULT_CELL_SIZE: f64 = 3.0;
    const DEFAULT_START_POSITION: (usize, usize) = (0, 0);
    const DEFAULT_BG_COLOR: Color = [1.0; 4];
    const DEFAULT_CELL_COLOR: Color =  [0.1, 0.6, 0.3, 1.0];


    pub struct PistonRendererSettings {
        pub cell_size: f64,
        pub position: ConwayCell,
        pub bg_color: Color,
        pub cell_color: Color,
    }


    impl PistonRendererSettings {
        pub fn default() -> Self {
            Self {
                cell_size: DEFAULT_CELL_SIZE,
                position: DEFAULT_START_POSITION.into(),
                bg_color: DEFAULT_BG_COLOR,
                cell_color: DEFAULT_CELL_COLOR
            }
        }
    }

}


pub mod renderer_builder {
    use super::renderer_settings::PistonRendererSettings;
    use super::piston::window::{WindowSettings, Size, Window};
    use super::opengl_graphics::OpenGL;
    use super::PistonRenderer;

    use super::piston::window::OpenGLWindow;
    use super::piston_window::BuildFromWindowSettings;


    pub struct PistonRendererBuilder
    {
        starting_settings: PistonRendererSettings,
        window_settings: WindowSettings,
    }

    impl PistonRendererBuilder
    {
        pub fn new<T: Into<String>, S: Into<Size>>(title: T, size: S) -> Self {
            let starting_settings = PistonRendererSettings::default();

            Self {
                window_settings: WindowSettings::new(title, size),
                starting_settings: PistonRendererSettings::default(),
            }

        }

        pub fn exit_on_esc(mut self, value: bool) -> Self {
            self.window_settings.set_exit_on_esc(value);
            self
        }

        pub fn opengl(mut self, value: OpenGL) -> Self {
            self.window_settings.set_opengl(value);
            self
        }

        pub fn fullscreen(mut self, value: bool) -> Self {
            self.window_settings.set_fullscreen(value);
            self
        }

        pub fn build<W>(mut self) -> Result<PistonRenderer<W>, String>
            where W: BuildFromWindowSettings + Window + OpenGLWindow
        {
            //self.window_settings.set_opengl(self.opengl_version);

            let window_result = self.window_settings.build();

            match window_result {
                Ok(window) => {
                    Ok(PistonRenderer::from_settings(self.starting_settings, window))
                }
                Err(err) => { Err(err) }
            }

        }
    }
}


use self::renderer_settings::PistonRendererSettings;

pub struct PistonRenderer<W:Window+OpenGLWindow=GlutinWindow> {
    window: RefCell<PistonWindow<W>>,
    settings: PistonRendererSettings,
}


impl<W:Window+OpenGLWindow> PistonRenderer<W> {
    pub fn from_settings(settings: PistonRendererSettings, window: PistonWindow<W>) -> Self {
        Self {
            settings: settings,
            window: RefCell::new(window)
        }
    }

    pub fn render_cell(&self, cell: &ConwayCell) {
        let block = rectangle::square(0.0, 0.0, self.settings.cell_size as f64);

    }

    pub fn render_state(&self, state: Box<StateInterface>, context:&Context, graphics:&mut G2d) {
        let view_size = context.get_view_size();
        let (w, h) = (view_size[0] as usize, view_size[1] as usize);

        clear(self.settings.bg_color, graphics);
        let rect = Rect {
            x: self.settings.position.x, y: self.settings.position.y,
            width: w/(self.settings.cell_size as usize), height: h/(self.settings.cell_size as usize)
        };

        for cell in state.iter_cells_in_rect(rect) {
            let screen_x = (cell.x - self.settings.position.x) as f64;
            let screen_y = (cell.y - self.settings.position.y) as f64;

            let block = rectangle::square(0.0, 0.0, self.settings.cell_size as f64);

            rectangle(
                self.settings.cell_color, block,
                context.transform.trans(
                    screen_x * self.settings.cell_size,
                    screen_y * self.settings.cell_size,
                ),
                graphics
            );
        }
    }


    pub fn render_loop(&mut self, starting_state: Box<StateInterface>) {
        let mut state_iter = starting_state.into_iter();

        let mut window = self.window.borrow_mut();

        while let Some(e) = window.next() {
            window.draw_2d(
                &e, |c, mut g| {
                    self.render_state(state_iter.next().unwrap(), &c, &mut g);
                }
            );
        }

    }
}