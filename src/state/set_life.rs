use std::collections::HashSet;
use state::state::{State, StateInterface, StateRandomizable, StateBase};
use cell::{ConwayCell, Rect};

// Conway field as a hash set
pub type ConwayField = HashSet<ConwayCell>;


trait ConwayFieldTrait {
    fn get_intersection(&self, other: &Self) -> Self;
    fn get_union(&self, other: &Self) -> Self;
    fn get_difference(&self, other: &Self) -> Self;
}

impl ConwayFieldTrait for ConwayField {
    fn get_intersection(&self, other: &ConwayField) -> ConwayField {
        self.intersection(other).cloned().collect()
    }

    fn get_union(&self, other: &ConwayField) -> ConwayField {
        self.union(other).cloned().collect()
    }

    fn get_difference(&self, other: &ConwayField) -> ConwayField {
        self.difference(other).cloned().collect()
    }
}

//

#[derive(Debug)]
pub struct SetLifeConwayState {
    conway_base: StateBase,
    cells: ConwayField,
}


impl SetLifeConwayState {

    pub fn with_base(base: StateBase) -> Self {
        Self {
            cells: ConwayField::new(),
            conway_base: base,
        }
    }

    fn cell_neighbours(&self, cell:&ConwayCell) -> ConwayField {
        let its: [(i8, i8); 8] = [
            (-1, -1), (0, -1), (1, -1),
            (-1,  0),          (1,  0),
            (-1,  1), (0,  1), (1,  1)
        ];

        let mut result = ConwayField::new();

        for &(dx, dy) in its.into_iter() {

            //let newx = match dx {
            //    -1 => { cell.x.checked_sub(1) },
            //     1 => { cell.x.checked_add(1) },
            //     _ => { Some(cell.x) }
            //};

            let newx = if dx>0 {
                cell.x.checked_add(dx as usize)
            } else { cell.x.checked_sub(-dx as usize) };

            let newy = match dy {
                -1 => { cell.y.checked_sub(1) },
                1 => { cell.y.checked_add(1) },
                _ => { Some(cell.y) }
            };

            if let (Some(x), Some(y)) = (newx, newy) {
                let cell = ConwayCell { x: x, y: y };
                if self.conway_base.layout.is_cell_allowed(&cell) {
                    result.insert(cell);
                }
            }

        }

        result
    }

    fn process_cell(&self, cell: &ConwayCell, is_alive: bool) -> ConwayField {
        let mut result = ConwayField::new();
        let nearby_area = self.cell_neighbours(&cell);
        let nearby_live_cells = nearby_area.get_intersection(&self.cells);
        let nearby_live_cells_count = nearby_live_cells.len();

        if nearby_live_cells_count == 3 || (is_alive && nearby_live_cells_count == 2) {
            result.insert(cell.clone());
        }

        if is_alive {
            let dead_cells = nearby_area.get_difference(&nearby_live_cells);
            for dead_cell in dead_cells.iter() {
                let additional_cells = self.process_cell(dead_cell, false);
                result = result.get_union(&additional_cells);
            }
        }

        result
    }
}


impl State for SetLifeConwayState {
    fn _add_cell(&mut self, cell:ConwayCell) {
        self.cells.insert(cell);
    }

    fn get_base(&self) -> &StateBase {
        &self.conway_base
    }

    fn get_mut_base(&mut self) -> &mut StateBase {
        &mut self.conway_base
    }

}

impl StateInterface for SetLifeConwayState {
    fn iter_cells<'s>(&'s self) -> Box<Iterator<Item=ConwayCell> + 's> {
        Box::new( self.cells.iter().map(|x| {x.clone()}) )
    }

    fn iter_cells_in_rect<'s>(&'s self, rect: Rect) -> Box<Iterator<Item=ConwayCell> + 's> {
        Box::new(
            self.cells
                .iter()
                .filter(move |cell| {(&rect).contains(&cell)})
                .map(|x| {x.clone()} )
        )
    }

    fn get_next_state(&self) -> Box<StateInterface > {
        let mut new_layout = ConwayField::new();

        for cell in self.cells.iter() {
            new_layout = new_layout.get_union(&self.process_cell(cell, true));
        }

        Box::new(
            SetLifeConwayState {
                cells: new_layout,
                conway_base: self.conway_base.next_generation(),
            }
        )
    }

}


impl StateRandomizable for SetLifeConwayState {}

