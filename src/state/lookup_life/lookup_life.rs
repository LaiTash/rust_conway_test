extern crate bit_vec;

use state::state::{State, StateBase};
use layout::Layout;
use cell::{ConwayCell, Rect};
use std::cmp::min;
use super::solve_table::{SolveTable};
use std::rc::Rc;


pub type Field = Vec<bool>;


pub struct LookupLifeState {
    pub conway_base: StateBase,
    pub solve_table: Rc<SolveTable>,
    pub field: Field,
}

fn get_required_capacity(layout: &Layout) -> usize {
    ((layout.max_height() + 1) * (layout.max_width() + 2)) as usize
}



pub struct CellIter<'a> {
    /// Cell iterator for lookup state
    state: &'a LookupLifeState,
    i: usize,
    x: usize,
    y: usize,
    rect: Rect,
    end_i: usize,
}

impl<'a> CellIter<'a> {
    fn create(state: &'a LookupLifeState, rect: Rect) -> CellIter<'a> {
        let start_i = state.get_i_at(rect.x, rect.y);
        let end_i = min(
            state.get_i_at(rect.x + rect.width, rect.y + rect.height),
            state.field.len() - state.conway_base.layout.max_width() - 2
        );

        CellIter {
            state: state,
            i: start_i,
            rect: rect,
            end_i: end_i,
            x: rect.x,
            y: rect.y,
        }
    }
}


impl<'a> Iterator for CellIter<'a> {
    type Item = ConwayCell;

    fn next(&mut self) -> Option<Self::Item> {
        let mut result = None;
        let max_x = min(
            self.rect.x + self.rect.width,
            self.state.get_base().layout.max_width() - 1
        );

        while result == None && self.i < self.end_i {
            if *self.state.field.get(self.i).unwrap() {
                let cell = ConwayCell {x:self.x, y:self.y};
                result = Some(cell)
            }

            self.x += 1;
            self.i += 1;

            if self.x > max_x {
                self.y += 1;
                self.x = self.rect.x;
                self.i = self.state.get_i_at(self.x, self.y);
            }
        }

        result
    }

}


impl LookupLifeState {

    pub fn with_base(base: StateBase, solve_table: Rc<SolveTable>)
        -> LookupLifeState
    {
        LookupLifeState {
            solve_table: solve_table,
            field: vec![false; get_required_capacity(&*base.layout)],
            conway_base: base,
        }
    }

    pub fn get_i_at(&self, x:usize, y:usize) -> usize {
        y * (self.conway_base.layout.max_width()+2) + x + 1
    }

    #[inline]
    pub fn apply_three(&self, lookup: &mut u16, i: usize){
        *lookup <<= 3;
        *lookup &= 511;
        //println!("{}, {}", val, lookup);
        *lookup
            |= (self.field[i] as u16) << 2
            | (self.field[i+1] as u16) << 1
            | (self.field[i+2] as u16);
        //println!("{}, {}, {}, {}, {}", val, lookup, self.field[i], self.field[i+1], self.field[i+2]);
    }

    pub fn iter_cells<'s>(&'s self) -> CellIter<'s> {
        CellIter::create(
            self,
            Rect {
                x: 0, y:0,
                width: self.get_base().layout.max_width(),
                height: self.get_base().layout.max_height()
            }
        )
    }

    pub fn iter_cells_rect<'s>(&'s self, rect:Rect) -> CellIter<'s> {
        CellIter::create(self, rect)
    }

    pub fn get_required_capacity(&self) -> usize {
        get_required_capacity(&*self.conway_base.layout)
    }

}


impl State for LookupLifeState {
    fn _add_cell(&mut self, cell: ConwayCell) {
        let w = self.conway_base.layout.max_width() + 2;
        let h = self.conway_base.layout.max_height() + 1;

        let i = cell.y * w + cell.x;
        self.field[i] = true;
    }

    fn get_base(&self) -> &StateBase {
        &self.conway_base
    }

    fn get_mut_base(&mut self) -> &mut StateBase {
        &mut self.conway_base
    }
}


