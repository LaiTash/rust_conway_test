pub mod solve_table;

pub mod lookup_life;

pub mod naive;
pub mod threaded;