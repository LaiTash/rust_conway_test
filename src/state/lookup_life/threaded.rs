extern crate scoped_pool;


use cell::Rect;
use super::super::super::cell::{ConwayCell};
use super::super::state::{StateBase, StateInterface, StateRandomizable, State};
use super::lookup_life::{LookupLifeState, CellIter, Field};
use super::solve_table::{SolveTable};
use std::sync::{Arc, Mutex};
use self::scoped_pool::Pool;
use std::rc::Rc;


pub struct ThreadedLifeState {
    state: LookupLifeState,
    thread_pool: Rc<Pool>,
}


pub fn apply_three(field: &Field, lookup: &mut u16, i: usize){
    *lookup <<= 3;
    *lookup &= 511;
    //println!("{}, {}", val, lookup);
    *lookup
        |= (field[i] as u16) << 2
        | (field[i+1] as u16) << 1
        | (field[i+2] as u16);
    //println!("{}, {}, {}, {}, {}", val, lookup, self.field[i], self.field[i+1], self.field[i+2]);
}


impl ThreadedLifeState {
    pub fn with_base(base: StateBase, solve_table: Rc<SolveTable>, thread_pool: Rc<Pool>)
                     -> ThreadedLifeState
    {
        Self {
            state: LookupLifeState::with_base(base, solve_table),
            thread_pool: thread_pool.clone(),
        }
    }

    pub fn iter_cells<'s>(&'s self) -> CellIter<'s> {
        self.state.iter_cells()
    }

    pub fn iter_cells_rect<'s>(&'s self, rect:Rect) -> CellIter<'s> {
        self.state.iter_cells_rect(rect)
    }

    pub fn get_next_state(&self) -> ThreadedLifeState {
        let mut result_field = vec![false; self.state.get_required_capacity()];

        let max_width = self.state.conway_base.layout.max_width() as usize;
        let max_height = self.state.conway_base.layout.max_height() as usize;

        let mut top_i = 0;

        {
            let rf_arc = Arc::new(Mutex::new(&mut result_field));
            let st_arc = Arc::new(Mutex::new(&*self.state.solve_table));
            let field = &self.state.field;

            self.thread_pool.scoped(
                |scope| {
                    for x in 0..max_width {
                        top_i += 1;

                        let rf = rf_arc.clone();
                        let st = st_arc.clone();

                        scope.execute(move || {
                            let mut i = top_i;
                            let mut lookup_index = 0;
                            apply_three(field, &mut lookup_index, i - 1);

                            for y in 0..max_height {
                                apply_three(field, &mut lookup_index, i + max_width + 1);
                                rf.lock().unwrap()[i] = st.lock().unwrap()[lookup_index as usize];
                                i += max_width + 2;
                            }
                        })
                    }
                }
            );
        }

        Self {
            state: LookupLifeState {
                solve_table: self.state.solve_table.clone(),
                field: result_field,
                conway_base: self.state.conway_base.next_generation(),
            },
            thread_pool: self.thread_pool.clone(),
        }
    }
}


impl StateInterface for ThreadedLifeState {
    fn iter_cells<'s>(&'s self) -> Box<Iterator<Item=ConwayCell> + 's> {
        Box::new(
            ThreadedLifeState::iter_cells(self)
        )
    }

    fn iter_cells_in_rect<'s>(&'s self, rect: Rect) -> Box<Iterator<Item=ConwayCell> + 's> {
        Box::new(
            ThreadedLifeState::iter_cells_rect(self, rect)
        )
    }

    fn get_next_state(&self) -> Box<StateInterface> {
        Box::new(
            ThreadedLifeState::get_next_state(self)
        )
    }
}


impl State for ThreadedLifeState {
    fn _add_cell(&mut self, cell: ConwayCell) {
        self.state._add_cell(cell)
    }

    fn get_base(&self) -> &StateBase {
        self.state.get_base()
    }

    fn get_mut_base(&mut self) -> &mut StateBase {
        self.state.get_mut_base()
    }
}


impl StateRandomizable for ThreadedLifeState {}
