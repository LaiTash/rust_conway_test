pub type SolveTable = [bool; 512];


pub fn generate_solve_table() -> SolveTable {
    let mut result: SolveTable = [false; 512];

    for i in 0usize..512 {
        let center_alive = (i & 16) == 16;
        let alive_cells = i.count_ones();
        result[i] =  alive_cells==3 || (center_alive && alive_cells == 4)
    }

    result
}