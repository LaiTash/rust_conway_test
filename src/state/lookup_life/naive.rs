use cell::Rect;
use super::super::super::cell::{ConwayCell};
use super::super::state::{StateBase, StateInterface, StateRandomizable, State};
use super::lookup_life::{LookupLifeState, CellIter};
use super::solve_table::{SolveTable};
use std::rc::Rc;


pub struct NaiveLifeState {
    state: LookupLifeState
}



impl NaiveLifeState {
    pub fn with_base(base: StateBase, solve_table: Rc<SolveTable>)
                     -> NaiveLifeState
    {
        NaiveLifeState {state: LookupLifeState::with_base(base, solve_table)}
    }

    pub fn iter_cells<'s>(&'s self) -> CellIter<'s> {
        self.state.iter_cells()
    }

    pub fn iter_cells_rect<'s>(&'s self, rect:Rect) -> CellIter<'s> {
        self.state.iter_cells_rect(rect)
    }

    pub fn get_next_state(&self) -> NaiveLifeState {
        let mut result_field = vec![false; self.state.get_required_capacity()];

        let max_width = self.state.conway_base.layout.max_width() as usize;
        let max_height = self.state.conway_base.layout.max_height() as usize;

        let mut top_i = 0;

        for _ in 0..max_width {
            top_i += 1;
            let mut i = top_i;
            let mut lookup_index = 0;
            self.state.apply_three(&mut lookup_index, i-1);

            for _ in 0..max_height {
                self.state.apply_three(&mut lookup_index, i+max_width+1);
                result_field[i] = self.state.solve_table[lookup_index as usize];
                i += max_width + 2;
            }
        }

        NaiveLifeState {
            state: LookupLifeState {
                solve_table: self.state.solve_table.clone(),
                field: result_field,
                conway_base: self.state.conway_base.next_generation(),
            }
        }
    }
}


impl StateInterface for NaiveLifeState {
    fn iter_cells<'s>(&'s self) -> Box<Iterator<Item=ConwayCell> + 's> {
        Box::new(
            NaiveLifeState::iter_cells(self)
        )
    }

    fn iter_cells_in_rect<'s>(&'s self, rect: Rect) -> Box<Iterator<Item=ConwayCell> + 's> {
        Box::new(
            NaiveLifeState::iter_cells_rect(self, rect)
        )
    }

    fn get_next_state(&self) -> Box<StateInterface> {
        Box::new(
            NaiveLifeState::get_next_state(self)
        )
    }
}


impl State for NaiveLifeState {
    fn _add_cell(&mut self, cell: ConwayCell) {
        self.state._add_cell(cell)
    }

    fn get_base(&self) -> &StateBase {
        self.state.get_base()
    }

    fn get_mut_base(&mut self) -> &mut StateBase {
        self.state.get_mut_base()
    }
}


impl StateRandomizable for NaiveLifeState {}
