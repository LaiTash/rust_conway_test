extern crate rand;

use cell::{ ConwayCell, ConwayCellCoord, Rect };
use layout::Layout;
use std::mem::replace;
use self::rand::Rng;
use std::rc::Rc;


// State iterator
pub struct StateIterator {
    state: Box<Box<StateInterface>>
}


impl Iterator for StateIterator {
    type Item = Box<StateInterface>;

    fn next(&mut self) -> Option<Self::Item> {
        let new_state = self.state.get_next_state();
        Some(replace(&mut self.state, new_state))
    }
}

impl IntoIterator for Box<StateInterface> {
    type Item = Box<StateInterface>;
    type IntoIter = StateIterator;

    fn into_iter(self) -> Self::IntoIter {
        Self::IntoIter {
            state: Box::new(self)
        }
    }
}

// State
#[derive(Debug)]
pub struct StateBase {
    pub layout: Rc<Layout>,
    pub generation: u64,
}

impl StateBase {
    pub fn with_layout(layout: Rc<Layout>) -> Self{
        Self {
            layout: layout.clone(),
            generation: 0
        }
    }

    pub fn next_generation(&self) -> Self{
        Self {
            layout: self.layout.clone(),
            generation: self.generation + 1
        }
    }
}


pub trait State {
    fn _add_cell(&mut self, cell: ConwayCell);
    fn get_base(&self) -> &StateBase;
    fn get_mut_base(&mut self) -> &mut StateBase;
}


pub trait StateRandomizable: StateInterface {
    fn randomize<RNGT>(&mut self, rng: &mut RNGT, max_cells: u64) where RNGT: Rng {
        let max_w = self.get_base().layout.max_width();
        let max_h = self.get_base().layout.max_height();

        for _ in 0..max_cells {
            let x: ConwayCellCoord = rng.gen_range(0, max_w);
            let y: ConwayCellCoord = rng.gen_range(0, max_h);
            let cell = ConwayCell { x: x, y: y };
            self._add_cell(cell);
        }
    }
}


pub trait StateInterface: State {

    fn iter_cells<'s>(&'s self) -> Box<Iterator<Item=ConwayCell> + 's>;
    fn iter_cells_in_rect<'s>(&'s self, rect: Rect) -> Box<Iterator<Item=ConwayCell> + 's>;

    fn get_next_state(&self) -> Box<StateInterface>;

    fn add_cell(&mut self, cell: ConwayCell) -> bool {
        if self.get_base().layout.is_cell_allowed(&cell) {
            self._add_cell(cell);
            true
        } else {
            false
        }
    }

}
