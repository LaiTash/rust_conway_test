extern crate rand;
extern crate conway;
extern crate scoped_pool;
extern crate glutin_window;

use conway::layout::RectLayout;
use conway::state::state::{StateRandomizable, StateInterface, StateBase};
use conway::state::lookup_life::lookup_life::{LookupLifeState};
use conway::state::lookup_life::naive::NaiveLifeState;
use conway::state::lookup_life::threaded::ThreadedLifeState;
use conway::state::lookup_life::solve_table::{generate_solve_table};

use scoped_pool::Pool;
use std::rc::Rc;


use conway::state::set_life::SetLifeConwayState;
use rand::{StdRng, SeedableRng};

use conway::renderers::curses::CursesRenderer;
use conway::renderers::piston::PistonRenderer;
use conway::renderers::piston::renderer_builder::PistonRendererBuilder;



enum StateType {
    SetLife,
    Naive,
    Threaded(usize),
}


macro_rules! create_state {
    ($t:ident, $r:expr, $starting_cells:expr, $ ($x: expr),* ) => {
        {
            let mut starting_state = $t::with_base($($x),*);
            starting_state.randomize($r, $starting_cells);
            Box::new(starting_state)
        }
    };
}


fn create_state(state_type: StateType, size: usize, starting_cells: u64, seed: &[usize])
    -> Box<StateInterface>
    {
    let layout = RectLayout::squared(size);
    let state_base = StateBase::with_layout(Rc::new(layout));
    let mut rng = StdRng::from_seed(seed);

    match state_type {
        StateType::SetLife => {
            create_state!(SetLifeConwayState, &mut rng, starting_cells, state_base)
        }
        StateType::Naive => {
            let solve_table = Rc::new(generate_solve_table());
            create_state!(
                NaiveLifeState, &mut rng, starting_cells, state_base, solve_table
            )
        }
        StateType::Threaded(workers) => {
            let solve_table = Rc::new(generate_solve_table());
            let pool = Rc::new(Pool::new(workers));
            create_state!(
                ThreadedLifeState, &mut rng, starting_cells, state_base, solve_table, pool
            )
        }
    }
}


fn main() {
    let size = 2000;
    let starting_cells = 1500 * 1000;
    let state_type = StateType::Naive;
    let seed: &[_] = &[1,2,9,4];

    let state = create_state(state_type, size, starting_cells, seed);

    use glutin_window::GlutinWindow;
    let renderer_result: Result<PistonRenderer<GlutinWindow>, String>;
    renderer_result = PistonRendererBuilder::new("conway", [600, 600])
        .fullscreen(true)
        .exit_on_esc(true)
        .build();
    match renderer_result {
        Ok(mut renderer) => {
            renderer.render_loop(state)
        }
        Err(s) => {
            println!("{}", s)
        }
    }
    //renderer.render_loop(state);
    //PistonRenderer::new((600, 600), "conway").render_loop(state)
}
