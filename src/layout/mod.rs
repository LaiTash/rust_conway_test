use std::fmt::Debug;
use super::cell::{ ConwayCellCoord, ConwayCell };


pub trait Layout: Debug + Sync {
    fn max_width(&self) -> ConwayCellCoord;
    fn max_height(&self) -> ConwayCellCoord;
    fn is_cell_allowed(&self, cell: &ConwayCell) -> bool {
        let mw = self.max_width();
        let mh = self.max_height();

        let (x, y) = (cell.x, cell.y);
        let true_for_x = x < mw;
        let true_for_y = y < mh;
        true_for_x && true_for_y
    }
}


#[derive(Debug)]
pub struct RectLayout {
    width: ConwayCellCoord,
    height: ConwayCellCoord
}


impl RectLayout {
    pub fn rectangular(width: ConwayCellCoord, height: ConwayCellCoord) -> Self {
        Self { width: width, height: height}
    }

    pub fn squared(side: ConwayCellCoord) -> Self {
        Self { width: side, height: side }
    }

    pub fn maximal() -> Self {
        Self { width: 0, height: 0 }
    }
}

impl Layout for RectLayout {
    fn max_width(&self) -> ConwayCellCoord {
        match self.width {
            0 => { ConwayCellCoord::max_value() },
            v => { v }
        }
    }

    fn max_height(&self) -> ConwayCellCoord {
        match self.height {
            0 => { ConwayCellCoord::max_value() },
            v => { v }
        }
    }
}


